"""
Zetta Auth plugin for HTTPie.
"""
import os
from httpie.plugins import AuthPlugin
from requests.auth import AuthBase
import requests


__version__ = '0.0.1'
__author__ = 'Kenny York'
__licence__ = 'MIT'

class ZettaAuth(AuthBase):
  def __init__(self,key,secret):
    self.auth_url = os.getenv('ZETTA_AUTH_URL', 'http://centralite-test.apigee.net/jilia/v1/oauth/accesstoken')
    self.key = key
    self.secret = secret
    print vars(self)
  
  def __call__(self, r):
    auth = (self.key, self.secret)
    body = {'grant_type':'client_credentials'}
    
    print 'Making request to ' + self.auth_url
    res = requests.post(self.auth_url, auth=(self.key,self.secret), data=body)
    
    # throw an exception if the request failed
    res.raise_for_status()
    
    token = res.json()['access_token']
    print 'Received token: ' + token
    r.headers['Authorization'] = 'Bearer ' + token
    
    return r

class ZettaAuthPlugin(AuthPlugin):

  name = 'Zetta Auth'
  auth_type = 'zetta'
  description = ''

  def get_auth(self, username, password):
    return ZettaAuth(username, password)

if __name__ == "__main__":
  from pprint import pprint
  plugin = ZettaAuthPlugin()
  auth = plugin.get_auth('xHdfgc8uDko57pl7mdBgolwPjGj1KZmS','cACm7JcH3CbZuSpf')
  pprint(vars(auth(requests.Request())))