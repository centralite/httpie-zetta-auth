from setuptools import setup
try:
    import multiprocessing
except ImportError:
    pass


setup(
    name='httpie-zetta-auth',
    description='Zetta auth plugin for HTTPie.',
    version='0.0.1',
    author='Kenny York',
    author_email='kennyyork@centralite.com',
    license='MIT',
    url='https://bitbucket.org/centralite/httpie-zetta-auth',
    download_url='https://bitbucket.org/centralite/httpie-zetta-auth',
    py_modules=['httpie_zetta_auth'],
    zip_safe=False,
    entry_points={
        'httpie.plugins.auth.v1': [
            'httpie_zetta_auth = httpie_zetta_auth:ZettaAuthPlugin'
        ]
    },
    install_requires=[
        'httpie>=0.7.0',
    ],
)